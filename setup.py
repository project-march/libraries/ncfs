from setuptools import setup, find_packages

setup(
    name='ncfs',
    version='0.1.4',
    packages=find_packages(include=['NCFS']),
    install_requires=[
        'scipy>=1.9.3',
        'numpy>=1.14.5',
        'scikit-learn>=1.1.3',
    ],
)
